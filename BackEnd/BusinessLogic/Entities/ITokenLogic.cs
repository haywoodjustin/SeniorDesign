﻿using DataAccess.Models;

namespace BusinessLogic.Entities
{
    public interface ITokenLogic
    {
        Task<HostToken> GetHostToken();
        Task<Token> GetToken();
    }
}