﻿
using DataAccess.Data;
using DataAccess.Models;

namespace BusinessLogic.Entities;

public class TokenLogic : ITokenLogic
{
    private readonly ITokenData _tokenData;
    public TokenLogic(ITokenData tokenData)
    {
        _tokenData = tokenData;
    }

    public async Task<Token> GetToken()
    {
        var token = await _tokenData.GetToken();

        if (token == null) throw new ArgumentNullException("There are no tokens in the database");

        return token;
    }

    public async Task<HostToken> GetHostToken()
    {
        var token = await _tokenData.GetHostToken();

        if (token == null) throw new ArgumentNullException("There are no host tokens in the database");

        return token; 
    }
}
