﻿using DataAccess.Models;

namespace DataAccess.Data
{
    public interface IQueueData
    {
        Task AddSong(SongRequest song);
        Task RemoveSong(SongRequest song);
        Task<IEnumerable<SongRequest>> GetQueue();
        Task ClearQueue(); 
    }
}