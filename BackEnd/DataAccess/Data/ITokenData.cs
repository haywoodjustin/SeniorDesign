﻿using DataAccess.Models;

namespace DataAccess.Data;

public interface ITokenData
{
    Task AddHostToken(HostToken token);
    Task AddToken(Token token);
    Task DeleteHostToken();
    Task<HostToken> GetHostToken();
    Task<Token> GetToken();
}