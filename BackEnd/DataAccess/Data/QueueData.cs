﻿using DataAccess.DbAccess;
using DataAccess.Models;

namespace DataAccess.Data;

public class QueueData : IQueueData
{
    private readonly ISqlDataAccess _db;

    public QueueData(ISqlDataAccess db)
    {
        _db = db;
    }

    public async Task<IEnumerable<SongRequest>> GetQueue() =>
       await _db.LoadData<SongRequest, dynamic>("GetQueue", new { });

    public Task AddSong(SongRequest song) => 
        _db.SaveData("AddSong", song);

    public Task RemoveSong(SongRequest song) =>
        _db.SaveData("RemoveSong", new { SongName = song });

    public Task ClearQueue() =>
        _db.SaveData("ClearQueue", new { }); 
}

