﻿using DataAccess.DbAccess;
using DataAccess.Models;

namespace DataAccess.Data;

public class TokenData : ITokenData
{
    private readonly ISqlDataAccess _db;

    public TokenData(ISqlDataAccess db)
    {
        _db = db;
    }

    public Task AddHostToken(HostToken token) =>
        _db.SaveData("AddHostToken", token);

    public async Task<HostToken> GetHostToken()
    {
        var result = await _db.LoadData<HostToken, dynamic>("GetHostToken", new { });

        return result.FirstOrDefault();
    }

    public Task DeleteHostToken() =>
         _db.SaveData("DeleteHostToken", new { });

    public Task AddToken(Token token) =>
        _db.SaveData("AddToken", token);

    public async Task<Token> GetToken()
    {
        var result = await _db.LoadData<Token, dynamic>("GetToken", new { });

        return result.FirstOrDefault();
    }


}
