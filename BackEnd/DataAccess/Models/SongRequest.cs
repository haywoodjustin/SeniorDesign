﻿
namespace DataAccess.Models;

public class SongRequest
{
    public string? SongName { get; set; }
    public string? SongArtist { get; set; }
    public string? SongArt { get; set; }
    public string? SongURI { get; set; }
}
