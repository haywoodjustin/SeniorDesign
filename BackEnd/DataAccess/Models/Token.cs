﻿
namespace DataAccess.Models;

public class Token
{
    public string? cc_token { get; set; }

    public int partyID { get; set; }

    public float addNowMilliseconds { get; set; }

    public int id { get; set; }
}
