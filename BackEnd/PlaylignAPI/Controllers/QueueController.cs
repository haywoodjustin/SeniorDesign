﻿
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;

namespace PlaylignAPI.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class QueueController : ControllerBase
{
    private readonly IQueueData _queueData; 
    public QueueController(IQueueData queueData)
    {
        _queueData= queueData;
    }

    [HttpPost]
    public async Task<SongRequest> AddSong(SongRequest request)
    {
        try
        {
            await _queueData.AddSong(request);
            return request;
        }
        catch (Exception)
        {
            throw;
        }
    }

    [HttpGet]
    public async Task<IEnumerable<SongRequest>> GetQueue()
    { 
        return await _queueData.GetQueue();
    }

    [HttpDelete]
    public async Task<IResult> ClearQueue()
    {
        try
        {
            await _queueData.ClearQueue();
            return Results.Ok();
        }
        catch (Exception ex)
        {
            return Results.Problem(ex.Message);
        }
    }

    [HttpDelete]
    public async Task<SongRequest> RemoveSong(SongRequest song)
    {
        try
        {
            await _queueData.RemoveSong(song);
            return song;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
