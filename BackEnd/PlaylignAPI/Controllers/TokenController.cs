﻿using BusinessLogic.Entities;
using Microsoft.AspNetCore.Mvc;

namespace PlaylignAPI.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class TokenController : ControllerBase
{
    private readonly ITokenData _tokenData;
    private readonly ITokenLogic _tokenLogic; 

    public TokenController(ITokenData tokenData, ITokenLogic tokenLogic)
    {
        _tokenData = tokenData; 
        _tokenLogic = tokenLogic;
    }

    [HttpPost]
    public async Task<HostToken> AddHostToken(HostToken token)
    {
        try
        {
            await _tokenData.AddHostToken(token);
            return token; 
        }
        catch (Exception)
        {
            throw; 
        }
        
    }

    [HttpPost]
    public async Task<Token> AddToken(Token token)
    {
        try
        {
            await _tokenData.AddToken(token);
            return token;
        }
        catch (Exception)
        {
            throw; 
        }
    }

    [HttpGet]
    public async Task<HostToken> GetHostToken()
    {
        return await _tokenLogic.GetHostToken();
       
    }

    [HttpGet]
    public async Task<Token> GetToken()
    {
        return await _tokenLogic.GetToken();  
    }

    [HttpDelete]
    public async Task<IResult> DeleteHostToken()
    {
        try
        {
            await _tokenData.DeleteHostToken();
            return Results.Ok();
        }
        catch (Exception ex)
        {
            return Results.Problem(ex.Message);
        }
    }

}
