﻿
using Autofac.Extras.Moq;
using BusinessLogic.Entities;
using DataAccess.Data;
using DataAccess.Models;
using Moq;
using PlaylignAPI.Controllers;
using Xunit;

namespace UnitTests.BusinessLogic;

public  class TokenBusinessLogicTests
{
    [Fact]
    public async void GetHostToken_NoHostTokens_ThrowsNullException()
    {
        using (var mock = AutoMock.GetLoose())
        {
            mock.Mock<ITokenData>()
                .Setup(data => data.GetHostToken())
                .ReturnsAsync(() => null);

            var logic = mock.Create<TokenLogic>();

            await Assert.ThrowsAsync<ArgumentNullException>(() => logic.GetHostToken());

        }
    }
    [Fact]
    public async void GetToken_NoTokens_ThrowsNullException()
    {
        using (var mock = AutoMock.GetLoose())
        {
            mock.Mock<ITokenData>()
                .Setup(data => data.GetToken())
                .ReturnsAsync(() => null);

            var logic = mock.Create<TokenLogic>();

            await Assert.ThrowsAsync<ArgumentNullException>(() => logic.GetToken());

        }
    }
}
