﻿
using Autofac.Extras.Moq;
using BusinessLogic.Entities;
using DataAccess.Data;
using DataAccess.Models;
using Moq;
using PlaylignAPI.Controllers;
using Xunit;

namespace UnitTests.Controllers;

public class QueueControllerTests
{

    [Fact]
    public async Task GetQueue_WhenQueueExists_ReturnQueue()
    {
        using (var mock = AutoMock.GetLoose())
        {
            mock.Mock<IQueueData>()
                .Setup(data => data.GetQueue())
                .ReturnsAsync(GetSampleQueue());

            var controller = mock.Create<QueueController>();
            var expected = GetSampleQueue();

            var actual = await controller.GetQueue();

            Assert.True(actual != null);
            Assert.True(actual.Count() == expected.Count());
            
            for (var i = 0; i < actual.Count(); i++)
            {
                Assert.Equal(actual.ElementAt(i).SongArtist, expected.ElementAt(i).SongArtist);
                Assert.Equal(actual.ElementAt(i).SongArt, expected.ElementAt(i).SongArt);
                Assert.Equal(actual.ElementAt(i).SongArtist, expected.ElementAt(i).SongArtist);
                Assert.Equal(actual.ElementAt(i).SongURI, expected.ElementAt(i).SongURI);
            }
        }
    }

    [Fact]
    public async Task AddSong_ValidSong_ReturnsSong()
    {
        using (var mock = AutoMock.GetLoose())
        {
            var song = GetSampleSong();

            mock.Mock<IQueueData>()
                .Setup( data => data.AddSong(song));

            var controller = mock.Create<QueueController>();
            var expected = GetSampleSong();

            var actual = await controller.AddSong(song);

            Assert.True(actual != null);
            Assert.Equal(expected.SongName, actual.SongName); 
            Assert.Equal(expected.SongName, actual.SongName); 
            Assert.Equal(expected.SongName, actual.SongName); 
            Assert.Equal(expected.SongArt, actual.SongArt);
        }
    }

    private SongRequest GetSampleSong()
    {
        SongRequest song = new SongRequest
        {
            SongArt = "bla bla bla",
            SongArtist = "Morgan Wallen",
            SongName = "Whiskey Glasses",
            SongURI = "URI"
        };

        return song; 
    }

    private IEnumerable<SongRequest> GetSampleQueue()
    {
        IEnumerable<SongRequest> queue = new List<SongRequest>
        {
            new SongRequest
            {
                SongArt= "bla",
                SongArtist= "Bon Jovi",
                SongName= "Wanted Dead or Alive",
                SongURI= "URI"
            },
            new SongRequest
            {
                SongArt= "bla bla",
                SongArtist= "The Band Camino",
                SongName= "Roses",
                SongURI= "URI"
            },
            new SongRequest
            {
                SongArt= "bla bla bla",
                SongArtist= "Morgan Wallen",
                SongName= "Whiskey Glasses",
                SongURI= "URI"
            },
            new SongRequest
            {
                SongArt= "bla bla bla bla",
                SongArtist= "Taylor Swift ",
                SongName= "Love Story",
                SongURI= "URI"
            },
        };

        return queue; 
    }
}
