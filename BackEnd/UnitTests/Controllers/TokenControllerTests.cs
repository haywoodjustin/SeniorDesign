﻿
using Autofac.Extras.Moq;
using BusinessLogic.Entities;
using DataAccess.Data;
using DataAccess.Models;
using Moq;
using PlaylignAPI.Controllers;
using Xunit;

namespace UnitTests.Controllers;

public class TokenControllerTests
{
    [Fact]
    public async Task GetToken_ReturnsToken_WhenTokenIsAvailable()
    {
        using (var mock = AutoMock.GetLoose())
        {
            mock.Mock<ITokenLogic>()
                .Setup(logic => logic.GetToken())
                .ReturnsAsync(GetSampleToken());

            var controller = mock.Create<TokenController>();
            var expected = GetSampleToken();

            var actual = await controller.GetToken();

            Assert.True(actual != null);
            Assert.Equal(expected.cc_token, actual.cc_token);
            Assert.Equal(expected.addNowMilliseconds, actual.addNowMilliseconds);
            Assert.Equal(expected.id, actual.id);
            Assert.Equal(expected.partyID, actual.partyID);
        }
    }

    [Fact]
    public async Task GetHostToken_ReturnsHostToken_WhenHostTokenIsAvailable()
    {
        using (var mock = AutoMock.GetLoose())
        {
            mock.Mock<ITokenLogic>()
                .Setup(logic => logic.GetHostToken())
                .ReturnsAsync(GetSampleHostToken());

            var controller = mock.Create<TokenController>();
            var expected = GetSampleHostToken();

            var actual = await controller.GetHostToken();

            Assert.True(actual != null);
            Assert.Equal(expected.scope, actual.scope);
            Assert.Equal(expected.addNowMilliseconds, actual.addNowMilliseconds);
            Assert.Equal(expected.id, actual.id);
            Assert.Equal(expected.expires_in, actual.expires_in);
            Assert.Equal(expected.token_type, actual.token_type);
            Assert.Equal(expected.expires_in, actual.expires_in);
            Assert.Equal(expected.refresh_token, actual.refresh_token);
        }
    }

    [Fact]
    public async Task AddToken_TokenIsValid_ReturnToken()
    {
        using (var mock = AutoMock.GetLoose())
        {

            mock.Mock<ITokenData>()
                .Setup(data => data.AddToken(GetSampleToken())); 

            var controller = mock.Create<TokenController>();
            var expected = GetSampleToken();

            var actual = await controller.AddToken(GetSampleToken());

            Assert.True(actual != null);
            Assert.Equal(expected.cc_token, actual.cc_token);
            Assert.Equal(expected.addNowMilliseconds, actual.addNowMilliseconds);
            Assert.Equal(expected.id, actual.id);
            Assert.Equal(expected.partyID, actual.partyID);
        }
    }

    [Fact]
    public async Task AddHostToken_HostTokenIsValid_ReturnHostToken()
    {
        using (var mock = AutoMock.GetLoose())
        {

            mock.Mock<ITokenData>()
                .Setup(data => data.AddHostToken(GetSampleHostToken()));

            var controller = mock.Create<TokenController>();
            var expected = GetSampleHostToken();

            var actual = await controller.AddHostToken(GetSampleHostToken());

            Assert.True(actual != null);
            Assert.Equal(expected.scope, actual.scope);
            Assert.Equal(expected.addNowMilliseconds, actual.addNowMilliseconds);
            Assert.Equal(expected.id, actual.id);
            Assert.Equal(expected.expires_in, actual.expires_in);
            Assert.Equal(expected.token_type, actual.token_type);
            Assert.Equal(expected.expires_in, actual.expires_in);
            Assert.Equal(expected.refresh_token, actual.refresh_token);
        }
    }

    private  Token GetSampleToken()
    {
        Token token = new Token
        {
            addNowMilliseconds = 1000,
            cc_token = "Random Token",
            id = 1,
            partyID = 2
        };

        return token; 
    }

    private HostToken GetSampleHostToken()
    {
        HostToken token = new HostToken
        {
            addNowMilliseconds = 1000,
            access_token = "Random Token",
            id = 1,
            expires_in = 2,
            refresh_token = "Random Refresh",
            scope = "Random Scope",
            token_type = "Bearer"
        };

        return token;
    }
}
