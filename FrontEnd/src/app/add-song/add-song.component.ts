import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, EMPTY, Observable} from 'rxjs';
import { SongRequest } from '../song-request';
import { QueueService } from '../services/queue.service';
import { SearchService } from '../services/search.service';
import { RetrievalService } from '../services/retrieval.service';
import { Token } from '../token';
import { UpdateService } from '../services/update.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-add-song',
  templateUrl: './add-song.component.html',
  styleUrls: ['./add-song.component.scss'],
  providers: [] 
})
export class AddSongComponent implements OnInit{

  protected search = new FormControl();  
  songs$ = new Observable<SongRequest[]>(); 
  protected selectedSong: SongRequest | undefined; 
  private tokenEndpoint : string = '/token';

  constructor(private qs: QueueService, private ss: SearchService, private RS: RetrievalService, private US: UpdateService, private router: Router) { }

  ngOnInit(): void { 

    this.RS.get<Token[]>('/Token/GetToken').subscribe({
      next: data => console.log(data),
      error: er => this.RS.getNewToken()
    })


    // sets local observable equal to service observable 
    // whens service uses "next" this observable picks it up and displays it 

    //TODO: Route handling based on if the route is from host login or user joing 
    console.log(this.router.url) //logs URL, useful for looking at host login params 

    this.songs$ = this.ss.songs$;
    
    //subscribe to value change of search form and fire get songs after debounce time 
    this.search.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged()
      ).subscribe(term => {
        this.ss.getSongs(term); 
      });  
  }
 
  //TODO Reset form control and unselect clicked row on table 

  selectSong(song: SongRequest){
    this.qs.addSong(song);
  }

  //checks tokens to see if expired
  removeExpiredTokens(getCurrTokens: Token[]){
    let tokenExpireTime = Date.now() -3600000; //tokens expire an hour after creation
    for (let i = 0; i < getCurrTokens.length; i++) {
      //check that token was made in the last hour - if not, delete from db
      if (getCurrTokens[i].addNowMilliseconds < tokenExpireTime){
        console.log("Deleting expired tokens")
        this.US.removeToken("Token", getCurrTokens[i]);
      }
    }
    //if we get here and there are no tokens in db, make one. Otherwise, set token to last in db
    if (getCurrTokens.length <= 0 || getCurrTokens == null){
      console.log("Valid token does not exist - making a new one")
      this.RS.getNewToken();
    }
    else{
      console.log("Valid token already exists in db")
    }
    
    //at this point, there should be a valid token in the db to use
  }
}
