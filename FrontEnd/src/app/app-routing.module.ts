import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddSongComponent } from './add-song/add-song.component';
import { QueueComponent } from './queue/queue.component';
import { UserSelectionComponent } from './user-selection/user-selection.component';
import { CallbackComponent } from './callback/callback.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
  { path: '', component: UserSelectionComponent}, 
  { path: 'queue', component: QueueComponent},
  { path: 'addSong', component: AddSongComponent},
  { path: 'callback', component: CallbackComponent},
  { path: '**', pathMatch: 'full', component: ErrorPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
