import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QueueComponent } from './queue/queue.component';
import { AddSongComponent } from './add-song/add-song.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';  
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserSelectionComponent } from './user-selection/user-selection.component';
import { CallbackComponent } from './callback/callback.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { MatProgressBarModule } from '@angular/material/progress-bar'; 


@NgModule({
  declarations: [
    AppComponent,
    AddSongComponent,
    CallbackComponent,
    ErrorPageComponent,
    QueueComponent,
    UserSelectionComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule, 
    ReactiveFormsModule, 
    MatProgressBarModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
