import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { __values } from 'tslib';
import { RetrievalService } from '../services/retrieval.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {

  constructor(private activeRoute: ActivatedRoute, private router: Router, private RS: RetrievalService) {};

  ngOnInit(): void {
    this.activeRoute.queryParamMap.subscribe(params => {
      switch(params.keys[0]) {
        case "code": 
          this.handleCode(params.get('code')); 
          break;
        case "error":
          this.handleError(params);
          break; 
        default: 
          console.log('This did not work')
          break; 
      }
    })
  }

  handleError(param: Params){
    this.router.navigate(['/error']); 
  }

  async handleCode(code: string | null){
    console.log("handleCode was called")
    await this.RS.getHostTokenFromCode(code);
    console.log("code recieved"); 
    this.router.navigate(['/queue']);
  }

}
