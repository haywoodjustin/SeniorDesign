import { Component, OnDestroy, OnInit } from '@angular/core';
import { SongRequest } from '../song-request';
import { QueueService } from '../services/queue.service';
import { Observable, Subscription, switchMap, timer } from 'rxjs';
import { RetrievalService } from '../services/retrieval.service';
import { UpdateService } from '../services/update.service';
import { HostToken } from '../host_token';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.scss']
})
export class QueueComponent implements OnInit, OnDestroy {

  hostName = "not connected";

  queue$ = new Observable<SongRequest[]>(); 
  hostCurrSong$ = new Observable<string>();
  hostCurrArtist$ = new Observable<string>();
  hostCurrAlbumArt$ = new Observable<string>();
  hostCurrPlayPosition$ = new Observable<number>();
  

  hostTokenEndpoint = '/Token/GetHostToken';
  subscription: Subscription = new Subscription;

  constructor(private qs: QueueService, private rs: RetrievalService, private us: UpdateService, private AS: AuthService) {}

  ngOnInit(): void {
    this.getHostData();

    console.log("From Queue Component: " + this.AS.getTest()); 
    
    this.subscription = timer(0, 1000).pipe(
      switchMap(() => this.rs.get<HostToken>(this.hostTokenEndpoint))
    ).subscribe(data => {
      //console.log(data[0]['access_token'])
      this.rs.getHostPlayback(data.access_token);
    });
    // Setting observable equal to service observable 
    // This makes the local observable get updates when the service is updated 
    this.queue$ = this.qs.queue$;
    this.hostCurrSong$ = this.rs.hostCurrSong$;
    this.hostCurrArtist$ = this.rs.hostCurrArtist$;
    this.hostCurrAlbumArt$ = this.rs.hostCurrAlbumArt$;
    this.hostCurrPlayPosition$ = this.rs.hostCurrPlayPosition$;

  }

  ngOnChange(){
    this.getHostData();
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getHostData(){
    this.rs.get<HostToken>(this.hostTokenEndpoint).subscribe(data => {
      //console.log(data[0]['access_token'])
      this.rs.getHostData(data.access_token).subscribe(data => {
        //console.log(data.display_name)
        this.hostName = data.display_name;
      })
    })  
  }
}
