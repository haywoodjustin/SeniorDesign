import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({providedIn: 'root'})
export class AuthService {
    constructor(private Http : HttpClient){}
    private test = ""; 
    //Authentication 
    private loginUrl = "https://accounts.spotify.com/authorize"
    private client_id = "d0457e652a0e4e858ff8e183fa9ac877"
    private response_type = "code"
    private show_dialog = "true" 
    private scope = 'user-read-playback-state user-modify-playback-state';
    private redirect_uri = "http://localhost:4200/callback"
    private login =`${this.loginUrl}?client_id=${this.client_id}&scope=${this.scope}&response_type=${this.response_type}&redirect_uri=${this.redirect_uri}&show_dialog=${this.show_dialog}`

    onLogin(){
        console.log("This should have opened log in")
        this.test = "Changed on login"; 
        window.location.href = this.login;    
    }

    getTest(){
        return this.test; 
    }

} 