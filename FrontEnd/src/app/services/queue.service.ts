import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, map, tap } from "rxjs";
import { HostToken } from "../host_token";
import { SongRequest } from "../song-request";
import { RetrievalService } from "./retrieval.service";
import { UpdateService } from "./update.service";

@Injectable({providedIn: 'root'})
export class QueueService {

    private endpoint = '/Queue'; 
    private hostTokenEndpoint = '/Token/GetHostToken';

    private queueSubject: BehaviorSubject<SongRequest[]> = new BehaviorSubject<SongRequest[]>([]); 

    queue$: Observable<SongRequest[]> = this.queueSubject.asObservable(); 

    constructor(private RS: RetrievalService, private US: UpdateService) {
        console.log("Queue service created");
        this.getQueue();
    }
    
    getQueue(): void{
         this.RS.getQueue<SongRequest[]>(this.endpoint + "/GetQueue").subscribe({
            next: songs => this.queueSubject.next(songs),
            error: err => console.log("Error Getting Queue", err),
            complete: () => console.log("Done Getting Song Queue")
        })
    }

    addSong(song: SongRequest): void {
       this.US.updateQueue<SongRequest>(this.endpoint + "/AddSong", song).subscribe({
            next: (song: SongRequest) => 
            { 
                console.log("Song Added: ", song)
                this.RS.get<HostToken>(this.hostTokenEndpoint).subscribe((data: HostToken) =>{
                    console.log(typeof(data)); 
                    let token = data.access_token;
                    console.log(token);  
                    this.US.updateSpotifyHostQueue(song, token);
                })     
            },
            error: err => console.log("Error Adding To Queue", err),
            // if error, UI will not change and datbase will not update 
            complete: () => 
            { 
                // If complete, UI will update 
                let allSongs = this.queueSubject.getValue(); 
                allSongs.push(song); 
                this.queueSubject.next(allSongs); 
                console.log("Done Adding Song")
            }
        })
    }
}