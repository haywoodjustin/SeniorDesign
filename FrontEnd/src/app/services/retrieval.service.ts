import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Token } from "../token";
import { UpdateService } from "./update.service";
import { HostToken } from "../host_token";
import { BehaviorSubject, Observable } from "rxjs";
import { SongRequest, SpotifyHostPlayback } from "../song-request";

@Injectable({providedIn: 'root'})
export class RetrievalService {

    private uri: string = 'http://localhost:3000';
    private apiUrl: string = "https://localhost:7156/api"; 
    private client_id = "d0457e652a0e4e858ff8e183fa9ac877"
    private client_secret = 'aef7b013f4304106abb0afe3ef4dd9c5' // TODO hide this somewhere 
    private client_token_url = 'https://accounts.spotify.com/api/token'
    private redirect_uri = "http://localhost:4200/callback"
    private hostTokenEndpoint = '/Token/GetHostToken'; //consider making this a global variable

    private search_uri: string = "https://api.spotify.com/v1/search?type=track&q="

    private hostCurrSongSubject: BehaviorSubject<string> = new BehaviorSubject<string>(""); 
    hostCurrSong$: Observable<string> = this.hostCurrSongSubject.asObservable();

    private hostCurrAlbumArtSubject: BehaviorSubject<string> = new BehaviorSubject<string>(""); 
    hostCurrAlbumArt$: Observable<string> = this.hostCurrAlbumArtSubject.asObservable();

    private hostCurrArtistSubject: BehaviorSubject<string> = new BehaviorSubject<string>(""); 
    hostCurrArtist$: Observable<string> = this.hostCurrArtistSubject.asObservable();

    private hostCurrPlayPositionSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0); 
    hostCurrPlayPosition$: Observable<number> = this.hostCurrPlayPositionSubject.asObservable();


    constructor(private http: HttpClient, private US: UpdateService){}


    get<Type>(endpoint: string) { 
        //console.log("Get from RS was called")
        return this.http.get<Type>(this.apiUrl + endpoint)
    }

    getQueue<Type>(endpoint: string) { // from api
        //console.log("Get from RS was called")
        return this.http.get<Type>(this.apiUrl + endpoint)
    }

    getSong<Type>(token: string, searchString: string){
        console.log("Searching song with Spotify")
        let headerDict = {
            'Authorization': token
        }
          
        let requestOptions = {                                                                                                                                                                                 
            headers: new HttpHeaders(headerDict), 
        };
        return this.http.get(this.search_uri + searchString, requestOptions);
    
    }

    async getAuthToken(){
        console.log("Requesting client credential auth token")
        return await fetch(this.client_token_url, {
            method: 'POST',
            body: 'grant_type=client_credentials&client_id=' + this.client_id + '&client_secret=' + this.client_secret,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    }

    //gets token from spotify
    getNewToken(){
        this.getAuthToken().then(r => r.json())
        .then(r => {
            console.log(r.access_token)  
            // let milli: number = Date.now(); 
            let token : Token= {
                cc_token: "Bearer " + r.access_token,
                partyID: "1",
                addNowMilliseconds: Date.now(),
                id: 0 //this will be ignored by json-server, but need it for querying TODO maybe find a better way
            }
            // let token : Token= {
            //     cc_token: "Bearer " + r.access_token,
            //     partyID: "1",
            //     addNowMilliseconds: 168,
            //     id: 0 //this will be ignored by json-server, but need it for querying TODO maybe find a better way
            // }
            // console.log(token); 
            // console.log(typeof(token.addNowMilliseconds)); 
            this.US.updateToken(token);
        })
    }

    async getHostAuthToken(hostCode: string | null){
        console.log("Requesting authorization code token")
        let auth_url = "https://accounts.spotify.com/api/token"
        return await fetch(auth_url, {
            method: 'POST',
            body: 'grant_type=authorization_code&code=' + hostCode+ '&client_id=' + this.client_id + '&client_secret=' + this.client_secret + '&redirect_uri=' + this.redirect_uri,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                //'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
            }
        })
    }


    getHostTokenFromCode(hostCode: string | null){
        this.getHostAuthToken(hostCode).then(r => r.json())
            .then(r => {
                //console.log(r)
                let curr_host_token: HostToken={
                    access_token: "Bearer " + r.access_token,
                    token_type: r.token_type,
                    expires_in: r.expires_in,
                    refresh_token: r.refresh_token,
                    scope: r.scope,
                    addNowMilliseconds: Date.now(),
                    id: 0 //this number doesn't matter, it gets overwritten anyway. Just need it for mapping TODO possibly fix
                }
                this.US.updateHostToken(curr_host_token);
            })    
    }

    getHostPlayback(hostToken: string){
        //console.log("Getting host playback data from Spotify")
        //console.log(hostToken)
        let playbackUrl = "https://api.spotify.com/v1/me/player";
        
        let headerDict = {
            'Authorization': hostToken,
            'Content-Type': 'application/json'
        }
          
        let requestOptions = {                                                                                                                                                                                 
            headers: new HttpHeaders(headerDict), 
        };

        //return this.http.get(this.search_uri + searchString, requestOptions);
        this.http.get<SpotifyHostPlayback>(playbackUrl, requestOptions).subscribe({
            next: data => {
                //console.log("User Playback Data Retrieved: ", data);
                this.hostCurrSongSubject.next(data.item.name);
                let aList: string[] = [];
                let aString = "";
                if (data.item.artists.length > 1){
                    data.item.artists.map((a: any) =>{
                        aList.push(a.name)
                    })
                    aString = aList.join(", ")
                }
                else{ aString = data.item.artists[0]["name"]}
                this.hostCurrArtistSubject.next(aString);
                this.hostCurrAlbumArtSubject.next(data.item.album.images[1].url);
                //this.hostCurrPlayPositionSubject.next()
                //console.log(data.item.duration_ms)
                //console.log(data.progress_ms)
                let play_percent = Math.round((data.progress_ms / data.item.duration_ms)*100);
                this.hostCurrPlayPositionSubject.next(play_percent);
                //console.log(play_percent)

            },
            error: err => console.log("Error Getting User Playback Data: ", err),
            // if error, UI will not change and datbase will not update  TODO update this comment with whatever the fuck this does
            complete: () => 
            { 
                //console.log("can i get data here " + data)
                //this.hostCurrSongSubject.next()
                // If complete, UI will update 
                // /this.hostCurrAlbumArtSubject.
                // let allSongs = this.queueSubject.getValue(); 
                // allSongs.push(song); 
                // this.queueSubject.next(allSongs); 
                // console.log("Done Adding Song")
            }
        })
    }

    getHostData(hostToken: string){
        let hostDataUrl = "https://api.spotify.com/v1/me/";
        
        let headerDict = {
            'Authorization': hostToken,
            'Content-Type': 'application/json'
        }
          
        let requestOptions = {                                                                                                                                                                                 
            headers: new HttpHeaders(headerDict), 
        };

        //return this.http.get(this.search_uri + searchString, requestOptions);
        return this.http.get<any>(hostDataUrl, requestOptions)
    }

    //TODO do something with this i guess
    getHostSpotifyQueue(hostToken: string){
        let hostDataUrl = "https://api.spotify.com/v1/me/player/queue";
        
        let headerDict = {
            'Authorization': hostToken,
            'Content-Type': 'application/json'
        }
          
        let requestOptions = {                                                                                                                                                                                 
            headers: new HttpHeaders(headerDict), 
        };

        //return this.http.get<any>(hostDataUrl, requestOptions)
        this.http.get<any>(hostDataUrl, requestOptions).subscribe(data => {
            //console.log(data)
        })
    }


} 