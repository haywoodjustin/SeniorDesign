import { Injectable } from "@angular/core";
import { BehaviorSubject, catchError, delay, EMPTY, map, Observable, retry, shareReplay } from "rxjs";
import { SongRequest, JSONSongRequest, SpotifySongRequest } from "../song-request";
import { RetrievalService } from "./retrieval.service";
import { Token } from "../token";

@Injectable({providedIn: 'root'})
export class SearchService { 

    private tokenEndpoint = '/Token/GetToken';
    private songsSubject: BehaviorSubject<SongRequest[]> = new BehaviorSubject<SongRequest[]>([]); 
    private options = {headers : {}} 
    songs$: Observable<SongRequest[]> = this.songsSubject.asObservable();
    private activeToken = "";

    constructor(private RS: RetrievalService) {
        console.log("Retrieval service created");
        this.getTokensFromDb();
    }

    getSongs(searchText: string): void {
        if(!searchText) this.songsSubject.next([]); 
        else{
            this.RS.getSong<SpotifySongRequest[]>(this.activeToken, searchText).pipe(map(data =>{
                console.log(data); 
                console.log(this.activeToken)
                data = Object.values(data)[0]['items'];// as SpotifySongRequest[];
                return Object.values(data).map(song =>{
                    //console.log(song["artists"])
                    let aList: string[] = [];
                    let aString = "";
                    if (song["artists"].length > 1){
                        //console.log("test conditional")
                        //console.log(song["artists"])
                        song["artists"].map((a: any) =>{
                            aList.push(a["name"])
                        })
                        aString = aList.join(", ")
                    }
                    else{ aString = song["artists"][0]["name"]}
                    
                    return {songName: song["name"], songArtist: aString, songArt: song["album"]["images"][1]["url"], songURI: song["uri"] }
                })     
            }))
            .subscribe({
                next: data => {
                    console.log(data)
                    this.songsSubject.next(data)
                },
                error: err => {
                    console.log(err)
                    if (err.status == 401){
                        console.log("API token is likely expired. Getting new token")
                        this.RS.getNewToken();
                        this.getTokensFromDb();
                    }
                },
                complete: () => console.log("Done Getting Spotify Search")
            });
        }
    };

    doFilter(search: string, songs: JSONSongRequest[]): JSONSongRequest[]{
        return songs.filter(s => 
            s.title.toUpperCase().includes(search.toUpperCase()) || 
            s.artist.toUpperCase().includes(search.toUpperCase())
        )
    }

    //TODO: still getting error here, need to find a better way to retry this bitch
    getTokensFromDb(){
        this.RS.get<Token>(this.tokenEndpoint)
        .pipe(
            retry(3), // you retry 3 times
            delay(500), // each retry will start after 1 second,
            catchError((error) => {
                this.RS.getNewToken();
                throw new Error(error)
            })
            //atchError((err) => this.checkConnection(err))),
        )
        .subscribe(data => {
            //set token
            console.log("set token")
            this.activeToken = data.cc_token;
            // if (data.length == 0){
            //     this.RS.getNewToken();
            // }
            // else{
            //     this.activeToken = data[data.length -1].cc_token;
            // }
        })
    }
} 