import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Host, Injectable } from "@angular/core";
import { HostToken } from "../host_token";
import { Token } from "../token";
import { SongRequest } from "../song-request";

@Injectable({providedIn: 'root'})
export class UpdateService {

    private tokenEndpoint : string = '/Token/AddToken';
    private hostTokenEndpoint: string = '/Token/AddHostToken';
    private options = {headers: {'Content-Type' : 'application/json'}};
    private uri = "http://localhost:3000";
    private api_uri = "https://localhost:7156/api";
    private spotifyQueueEndpoint = "https://api.spotify.com/v1/me/player/queue";
    
    constructor(private http: HttpClient) {
        console.log("Update service created");
    }

    update<Type>(endpoint: string, data: Type){
        return this.http.post<Type>(endpoint, data, this.options)
    }

    updateQueue<Type>(endpoint: string, data: Type){
        return this.http.post<Type>(this.api_uri + endpoint, data, this.options)
    }

    delete<Type>(endpoint: string, data: Token | HostToken){
        //console.log(data.id)
        return this.http.delete<Type>(this.uri + endpoint + "/" + data.id)
    }

    updateToken(token: Token){
        //pushes token to db
        this.update<Token>(this.api_uri + this.tokenEndpoint, token).subscribe({
            next: code => console.log("Token Added: ", code),
            error: err => console.log("Error Adding To Token", err),
        })
    }

    updateHostToken(host_token: HostToken){
        console.log("updateHostToken called in US")
        console.log(host_token)
        this.http.post<HostToken>(this.api_uri + this.hostTokenEndpoint, host_token, this.options).subscribe({
            next: code => console.log("Host Token Added: ", code),
            error: err => console.log("Error Adding To Token", err),
        })
    }

    removeToken(tokentype: string, token: Token | HostToken){
        let chooseEndpoint = "";
        if(tokentype == "Token") chooseEndpoint = this.tokenEndpoint;
        else if (tokentype == "HostToken") chooseEndpoint = this.hostTokenEndpoint;
        this.delete<Token>(chooseEndpoint, token).subscribe({
            next: code => console.log(code),
            error: err => console.log("Error Deleting from Token", err),
        })
    }

    updateSpotifyHostQueue(song: SongRequest, hostToken: string){
        console.log(hostToken)
        let headerDict = {
            'Authorization': hostToken,
            'Content-Type': 'application/json'
        }
          
        let requestOptions = {                                                                                                                                                                                 
            headers: new HttpHeaders(headerDict), 
        };
        console.log("updateSpotifyHostQueue called from update service")
        this.http.post<string>(this.spotifyQueueEndpoint+ "?uri="+song.songURI, null, requestOptions).subscribe(data =>{
            console.log("Song was pushed to Spotify queue (hopefully")
        })
    }

}