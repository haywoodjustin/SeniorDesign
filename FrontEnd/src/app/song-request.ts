export interface SongRequest {
    songName: string;
    songArtist: string; 
    songArt: string;
    songURI: string;
}

export interface JSONSongRequest {
    title: string;
    artist: string;
    year: string;
    web_url: string;
    img_url: string; 
}

export interface album{
    url: string;
}
export interface artist {
    name: string;
}

export interface SpotifySongRequest {
    album: {
        name: string;
        images: album[];
        uri: string;
        //album_type: string;
    };
    artists: artist[];
    name: string;

}


export interface SpotifyHostPlayback {
    item: {
        album: {
            images: album[];
        }
        artists: artist[];
        name: string;
        duration_ms: number;
    }
    progress_ms: number;
}