export interface Token {
    cc_token: string;
    partyID: string; 
    addNowMilliseconds: number;
    id: number;
}