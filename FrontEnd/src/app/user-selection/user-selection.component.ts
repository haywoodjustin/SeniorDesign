import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.component.html',
  styleUrls: ['./user-selection.component.scss']
})
export class UserSelectionComponent  {

  constructor(private AS : AuthService) { }
  ngOnInit(): void {

  }
  
  onLoginClick(){
    //window.location.href = this.GS.getLogin(); 
    this.AS.onLogin(); 
  }  
}
