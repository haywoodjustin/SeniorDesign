# Assignment 1: Test Plans
Team: Slay

The main way we will be testing our app, Playlign, is through behavioral testing strategies. This works well for Playlign because features are designed to be user friendly and the more we use the app the more we realize where we need to add functionality or reduce complexity. This will also be an easy approach to getting feedback from other users outside of the team. Based on how someone who has never seen it reacts and interacts with it will tell us a lot about our design choices and next steps. 

Being able to completely test our app’s behavior at this stage will be very easy considering the limited capabilities and features of the app. Getting a full coverage test is quick and easy and tells us a lot in that short time. This strategy will also help us keep the app simple, considering we do not want to spend too much time testing features. Moving forward as more features and functions are added, along with a native API, unit tests can be built to ensure changes do not affect the functionality of the app. This added approach will require less manual testing in the long run in exchange for a little more heavy lifting in the process of building features. 

## Test case descriptions
1. Spotify Login Test
    - ID: api_spotify_login
    - Purpose: Make sure spotify login with token goes through as expected
    - Description: Using a Postman post request, send a request to Spotify and store the returned object
    - Inputs: Postman requests and variables: grant-type, client-id, client-secret
    - Outputs: API token response, status 200
    - Normal
    - Blackbox
    - Functional
    - Integration
2. Queue Update
    - ID: api_queue_update
    - Purpose: Make sure the database updates when songs are added
    - Description: Using a Postman get request, send a request to our database and view the returned queue object 
    - Inputs: Song request
    - Outputs: Song request object response, status 200 
    - Normal
    - Blackbox
    - Functional
    - Integration
3. Queue Update 
    - ID: ui_queue_update
    - Purpose: Make sure the user interface updates when songs are added to queue 
    - Description: In the app, load the queue page and compare the visualized queue to the database 
    - Inputs: Song request 
    - Outputs: Updated queue with expected values 
    - Normal
    - Whitebox
    - Functional
    - Integration
4. Token Update
    - ID: api_token_update
    - Purpose: Make sure token can be retrieved and updated in the database 
    - Description: Using a Postman post request, send a request to our database containing the Spotify token and compare returned value to the expected token value
    - Inputs: Spotify token
    - Outputs: Token object response, status 200 
    - Normal
    - Blackbox
    - Functional
    - Integration
5. Queue Get 
    - ID: api_queue_get
    - Purpose: Make sure we can still retrieve the queue from the database 
    - Description: Use Postman get request, send a request to our database and compare returned value to expected queue value
    - Inputs: Get request with “queue” endpoint value
    - Outputs: Queue retrieved from the database 
    - Normal
    - Blackbox
    - Functional
    - Integration
6. Queue Get 
    - ID: ui_get_queue
    - Purpose: Make sure the correct queue is retrieved and shown to the user 
    - Description: Check that all songs added by the user are shown in the queue
    - Inputs: User load page, list of already stored queue values
    - Outputs: User should be able to view queued songs, or no songs if no songs are queued yet
    - Normal
    - Whitebox
    - Functional
    - Integration
7. Token Get 
    - ID: api_get_token
    - Purpose: Validate that the authorization token is acquired and can be used to query the API
    - Description: Test using Postman request that request to get token works correctly and returns valid token
    - Inputs: Postman requests and variables: grant-type, client-id, client-secret
    - Outputs: Token response, status 200
    - Normal
    - Blackbox
    - Functional
    - Integration
8. Spotify Login Test
    - ID: ui_spotify_login
    - Purpose: Test to ensure user is able to login through Spotify successfully
    - Description: Ensure that user is redirected to a Spotify login page and successfully login with their Spotify credentials
    - Inputs: User Spotify credentials
    - Output: “Login Successful” screen
    - Normal
    - Whitebox
    - Functional
    - Integration
9. Add Song Test 
    - ID: ui_add_song
    - Purpose: Test user is able to add song to the queue
    - Description: Test different possible user actions [adding a song that exists, adding a song that doesn’t exist] and making sure that the song is both stored in the database and that it appears visually for the user
    - Inputs: User select song 
    - Output: Queue list updated with added song
    - Normal
    - Whitebox
    - Functional
    - Integration
10. Spotify Search Test 
    - ID: ui_spotify_search
    - Purpose: Validate that search bar operates correctly and can query songs
    - Description: Test different inputs to the search bar to make sure that songs can be searched, grabbing from Spotify’s song database
    - Inputs: User input, search page, query values
    - Outputs: Search page with dropdown values of valid queried songs, or search page with dropdown value of no songs matching the query
    - Normal
    - Whitebox
    - Functional
    - Integration
11. Spotify Search Test 
    - ID: api_ spotify_search
    - Purpose: Confirm that search returns a song values that are valid
    - Description: Use Postman request to test searches after an authorization token is passed
    - Inputs: Postman request and variables: access-token, track-type, query-value
    - Outputs: API search response, status 200
    - Normal
    - Blackbox
    - Functional
    - Integration

## Test Plan Matrix
| Test Case ID | Normal/Abnormal | Blackbox/Whitebox | Functional/Performance | Unit/Integration |
| ------ | ------ | ------ | ------ | ------ |
| api_spotify_login | Normal | Blackbox | Functional | Integration |
| api_queue_update | Normal | Blackbox | Functional | Integration |
| api_token_update | Normal | Blackbox | Functional | Integration |
| api_queue_get | Normal | Blackbox | Functional | Integration |
| api_get_token | Normal | Blackbox | Functional | Integration |
| api_ spotify_search | Normal | Blackbox | Functional | Integration |
| ui_queue_update | Normal | Whitebox | Functional | Integration |
| ui_get_queue | Normal | Whitebox | Functional | Integration |
| ui_spotify_login | Normal | Whitebox | Functional | Integration |
| ui_add_song | Normal | Whitebox | Functional | Integration |
| ui_spotify_search | Normal | Whitebox | Functional | Integration |


