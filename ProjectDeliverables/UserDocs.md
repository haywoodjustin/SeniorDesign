# User Documentation

### Setting up the Database to Run Locally 
1. Run this command on a command prompt to install the Json server globally 

    `$ npm install -g json-server`

2. There is a file named "db.json" located in the backend folder that this server will read from

3. Now we will run the server. In your VScode instance open a new terminal. Navigate to the BackEnd directory and enter this command: 

    `$ json-server --watch db.json`

4. To test that the server is running open a browser window and navigate to **localhost:3000**

5. Now that the database is configured and running correctly you can start using the app. When you are done make sure the go to the terminal running the databse and **enter ctrl + C to stop the server.** 
    - Note that the command in step 3 will need to be ran everytime to start the server and use the app.
6. Link to full instructions and more information on the capabilities of the Json server here: https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d 

### Running the App 
1. Download this project folder 

2. Open the project folder in Visual Studio Code 

3. In the terminal inside the front end folder run this command 

    `$ ng serve -o`

4. The app will open and you are ready to run 

### FAQ
- *Nothing is showing up in my queue, even after I ran the “json-server --watch db.json”!* 

    Ensure that the terminal that is running the json-server is within the BackEnd folder. This means changing directories from the main project “SeniorDesign” folder to the BackEnd folder. (The json server will not give an error even if you start it in the wrong folder.)

    `C:\Users\me\SeniorDesign> cd FrontEnd`

    `C:\Users\me\SeniorDesign\BackEnd> ng serve -o`

    Verify that the queue has items in it by going to http://localhost:3000/queue

- *I got this error: “Error: This command is not available when running the Angular CLI outside a workspace.” What should I do?*

    This may be another directory issue. Ensure that you run “ng serve” in the correct directory.

    C:\Users\me\SeniorDesign> cd FrontEnd
    C:\Users\me\SeniorDesign\FrontEnd> ng serve -o

### If the project is running correctly, you should get this message.
    ** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
    ✔ Compiled successfully.

