# Senior Design Project 
## Angular Spotify Companion App 

### Team Members:
- [Justin Haywood](/ProjectDeliverables/AboutProject/TeamMemberBios/JustinProfessionalBio.md)
- [Lina Kaval](/ProjectDeliverables/AboutProject/TeamMemberBios/Lina%20Kaval%20bio.md)
- [Samantha Shaw](/ProjectDeliverables/AboutProject/TeamMemberBios/SamanthaProfessionalBio.md)

### Project Description 
Our project is a web-based application that allows users to request songs to be played in group settings. The problem we are trying to fix is to make playing songs that everyone likes at a party easier and seamless. Users will navigate to the website and be able to request songs for the host to play. These songs will automatically be added to the host's queue, which is connected to Spotify.

### User Interface Specification
![image info](/ProjectDeliverables/ReadMeImages/playlign.png) ![image info](/ProjectDeliverables/ReadMeImages/queue.png) ![image info](/ProjectDeliverables/ReadMeImages/search.png)
- From the beginning of the design planning process, we discussed having a minimalistic design of our app. We aim to keep the design as simple as possible so that it would be easy for our users to use our app. With this design, we were hoping our application flows nicely to all of our users.
- Homepage
    - The homepage consists of our app logo and two buttons, giving users the option to login as a host or join as guest
- Queue Page
    - Upon login, the user will be directed to the queue page where they can see who the current host is, what song is currently playing, and the songs that have been added to the queue
- Search Page
    - At the top of the search page, users can find a back button which will redirect them back to the queue page and a search box next to it. When users start to type a song title or an artist, the list below the search box will populate and show users a list of songs related to what they typed in the search box.

### Table of Contents 

- [User Docs/Manual](/ProjectDeliverables/UserDocs.md)
- [Test Plans](/ProjectDeliverables/TestPlans.md)
- [User Stories](/ProjectDeliverables/AssignmentFour/UserStories.md)
- [Design Diagrams](/ProjectDeliverables/AssignmentFour/)
- [Task List](/ProjectDeliverables/AssignmentFive/TasksList.MD)
- [Milestone Timeline](/ProjectDeliverables/AssignmentSix/MilestonesTimelineAndEffort.pdf)
- [Constraints](/ProjectDeliverables/AssignmentSeven/ConstraintsEssay.MD)
- [Fall Final PPT](https://mailuc-my.sharepoint.com/:p:/g/personal/haywoojn_mail_uc_edu/Ec7mFosHIIFLguVTcroqhEcBs_8mRcBDhbb_ssyY5_pKIQ?rtime=iyhxWYY_20g)
- [Spring Final Demo](https://mailuc-my.sharepoint.com/personal/shaws8_mail_uc_edu/_layouts/15/stream.aspx?id=%2Fpersonal%2Fshaws8%5Fmail%5Fuc%5Fedu%2FDocuments%2FAttachments%2FPlaylignDemo%2Emp4&ct=1680749974677&or=OWA-NT&cid=3c53c414-cd00-0f92-0257-98bbe418392f&ga=1)
- [Expo Poster](/ProjectDeliverables/Senior%20Design%20Poster.pdf)
- [Meeting Notes](/ProjectDeliverables/MeetingNotes) 
- Fall Capstone Assessments 
    - [Justin](/ProjectDeliverables/AssignmentThree/CapstoneAssessmentJustin)
    - [Lina](/ProjectDeliverables/AssignmentThree/CapstoneAssessmentLina)
    - [Sam](/ProjectDeliverables/AssignmentThree/CapstoneAssessmentSam)
- Spring Capstone Assessments
    - [Justin](/ProjectDeliverables/FinalAssessment/Justin.md)
    - [Lina](/ProjectDeliverables/FinalAssessment/Lina.md)
    - [Sam](/ProjectDeliverables/FinalAssessment/Sam.md)


### Summary of Hours and Justification
![image info](/ProjectDeliverables/ReadMeImages/sprints.png)

#### Justin Haywood
|   | Hours |
|---|---|
| Fall Semester | 78.5 |
| Spring Semester | 20.5 |
| **TOTAL** | **99** |
Justin contributed to the project in the following ways: being Project Manager for the project, starting up the project in Angular, finding and creating the backend solution, setting up the routing for all the pages, creating every component/page, creating the callback page and catching the response, pair programming with Lina to create http requests for Spotify API, working on documentation to be submitted for class.

#### Lina Kaval
|   | Hours |
|---|---|
| Fall Semester | 24.5 |
| Spring Semester | 30.5 |
| **TOTAL** | **55** |
Lina contributed to the project in the following ways: creating the Spotify service to handle Spotify API calls, mapping the http responses to custom made objects, adding the functionality for displaying currently playing data, creating the progress bar for the song that was playing, creating new endpoints to save auth tokens, created the functionality of pushing songs from the local queue to the host's Spotify queue, working on documentation to be submitted for class.

#### Samantha Shaw
|   | Hours |
|---|---|
| Fall Semester | 18.5 |
| Spring Semester | 28.5 |
| **TOTAL** | **47** |
Samantha contributed to the project in the following ways: creating mockups of the UI/UX, choosing the color palette, creating custom button components, creating animations on user interactions, propogating and editing the CSS files, creating the logo, fixing the aesthetics of the queue, currently playing song information, and search functions, working on documentation to be submitted for class.


### Summary of Expenses
No expenses or donated hardware/software for this project.

### Appendix
1. [Commit Contributions on Gitlab](https://gitlab.com/haywoodjustin/SeniorDesign/-/graphs/main?ref_type=heads)
2. [Meeting Notes](/ProjectDeliverables/MeetingNotes/)